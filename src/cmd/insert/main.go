package main

import (
	"cs/src/libs/logs"
	"cs/src/model"
	"cs/src/services/courseService"
	"time"
)

func main() {
	logs.Info("开始写入数据数据")
	start := time.Now()

	course := courseService.Course{model.Course{Id: "1", Name: "测试", City_id: "1", Tid: "1", Week: "1", Classroom_id: "2", Campus_id: "2", Project_id: "3", Classtype_id: "4"}}
	courseService.RunInsert(&course)

	logs.Info("一共用时", time.Since(start))
}
