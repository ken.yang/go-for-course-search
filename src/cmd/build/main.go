package main

import (
	"cs/src/libs/logs"
	"cs/src/services/courseService"
	"time"
)

/**
应用初始化
*/
func init() {
	logs.Info("标题：这是一个课程算法")
}

/**
主函数
*/
func main() {
	start := time.Now()

	logs.Info("数据处理中......")

	courseService.RunBuild()

	logs.Info("总共用时：", time.Since(start))
}
