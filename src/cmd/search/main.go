package main

import (
	"cs/src/libs/logs"
	"cs/src/services/courseService"
	"time"
)

/**
应用初始化
*/
func init() {
	logs.Info("标题：课程搜索测试")
}

/**
主函数
*/
func main() {
	start := time.Now()

	//组装搜索条件
	var searchKey = make(map[string]string)
	searchKey["classroom_id"] = "1"
	searchKey["project_id"] = "1"

	//获取搜索条件和结果
	optionList := courseService.GetOptionList("1", "1", searchKey)
	searchList := courseService.GetSearchList("1", "1", searchKey)

	logs.Info("测试：查询条件：", searchKey)
	logs.Info("测试：查询条件结果：", optionList)
	logs.Info("测试：查询搜索结果：", searchList)

	logs.Info("总共用时：", time.Since(start))
}
