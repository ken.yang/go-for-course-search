package model

// 课程结构体
type Course struct {
	Id           string `json:"id"`
	Name         string `json:"name"`
	City_id      string `json:"city_id"`
	Tid          string `json:"tid"`
	Week         string `json:"week"`
	Classroom_id string `json:"classroom_id"`
	Campus_id    string `json:"campus_id"`
	Project_id   string `json:"project_id"`
	Classtype_id string `json:"classtype_id"`
}

// 根据keyName获取课程的值
func (c *Course) Get(keyName string) string {
	var keyValue string
	switch keyName {
	case "city_id":
		keyValue = c.City_id
		break
	case "tid":
		keyValue = c.Tid
		break
	case "week":
		keyValue = c.Week
		break
	case "classroom_id":
		keyValue = c.Classroom_id
		break
	case "campus_id":
		keyValue = c.Campus_id
		break
	case "project_id":
		keyValue = c.Project_id
		break
	case "classtype_id":
		keyValue = c.Classtype_id
		break
	}

	return keyValue
}
