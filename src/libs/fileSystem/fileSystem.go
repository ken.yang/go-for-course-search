package fileSystem

import (
	"cs/src/helper"
	"cs/src/libs/logs"
	"log"
	"os"
	"path/filepath"
)

/**
获取文件的路径
因为go的特性，go run，go build，go test会有不同的临时目录，那么os.Args[0]会获取到不同的值，为了适应不同的编译环境，所以需要多种判断方式
思路来源于beego源码
https://github.com/astaxie/beego/blob/master/config.go#L133-L146
*/
func GetFilePath(filename string) string {
	appPath, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	workPath, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	var filePath string

	appConfigPath := filepath.Join(appPath, filename)
	if !helper.FileExist(appConfigPath) {
		workConfigPath := filepath.Join(workPath, filename)
		if !helper.FileExist(workConfigPath) {
			log.Println("appConfigPath" + appConfigPath)
			log.Println("workConfigPath" + workConfigPath)
			log.Fatal("无法获取配置文件")
		} else {
			filePath = workConfigPath
		}
	} else {
		filePath = appConfigPath
	}

	logs.Loger.Println("当前文件绝对路径：" + filePath)
	return filePath
}
