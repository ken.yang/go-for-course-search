package output

/**
 * 封装的输出类，返回指定的格式信息
 *
 * @author ken
 * @date 2019-05-14
 * @version 0.2
 */

const (
	STATUS_SUCCESS = 200 //成功
	STATUS_ERROR   = 400 //失败
)

// 输出结构体
type Output struct {
	code    int
	message string
	data    interface{}
}

// 设置输出码code
func (op *Output) SetCode(code int) {
	op.code = code
}

// 设置信息message
func (op *Output) SetMessage(message string) {
	op.message = message
}

// 设置数据data
func (op *Output) SetData(data interface{}) {
	op.data = data
}

// 获取输出码code
func (op *Output) GetCode() int {
	return op.code
}

// 获取消息message
func (op *Output) GetMessage() string {
	return op.message
}

// 获取输出的数据data
func (op *Output) GetData() interface{} {
	return op.data
}

// 输出信息
func (op *Output) Info(data interface{}) map[string]interface{} {
	op.data = data
	return op.getAllData()
}

// 输出成功
func (op *Output) Success(data interface{}) map[string]interface{} {
	op.code = STATUS_SUCCESS
	op.message = "正确"
	op.data = data
	return op.getAllData()
}

// 输出错误
func (op *Output) Error(message string) map[string]interface{} {
	op.code = STATUS_ERROR
	if message != "" {
		op.message = message
	} else {
		op.message = "错误"
	}

	return op.getAllData()
}

// 组装为指定格式的基础数据
func (op *Output) getAllData() map[string]interface{} {
	data := make(map[string]interface{})
	data["message"] = op.message
	data["code"] = op.code
	data["data"] = op.data

	return data
}
