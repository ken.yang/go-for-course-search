package logs

import (
	"cs/src/helper"
	"io"
	"log"
	"os"
	"sync"
	"time"
)

var Loger *log.Logger

var logOnce sync.Once

/**
初始化
该文件保留了Info和Error方法，其实没有什么用，只是不想全局的修改代码文件
*/
func init() {
	logOnce.Do(func() {
		//创建输出日志文件
		logFilePath := "./logs/app-" + time.Now().Format("2006-01-02") + ".txt"

		if !helper.FileExist("./logs") {
			if ok := os.Mkdir("./logs", 0777); ok != nil {
				panic(ok)
			}
		}

		// 使用os.O_RDWR|os.O_APPEND|os.O_CREATE这种形式既可以创建文件，又可以追加，不用再去判断文件是否存在
		logFile, err := os.OpenFile(logFilePath, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0777)
		if err != nil {
			log.Fatal(err)
		}

		//创建一个Logger
		Loger = log.New(io.MultiWriter(logFile, os.Stderr), "【app】", log.Ldate|log.Lmicroseconds|log.Lshortfile)
	})
}

/**
打印info信息日志
*/
func Info(v ...interface{}) {
	Loger.Println(v)
}

/**
打印error错误日志
*/
func Error(v ...interface{}) {
	Loger.Fatalln(v)
}
