package test

import (
	"cs/src/libs/logs"
	"testing"
)

func TestLog(t *testing.T) {
	logs.Loger.Println("hello loger println") //这样记录的文件是运行的
	logs.Info("hello info")                   //这样记录的文件是当前的文件目录
}
