package test

import (
	"cs/src/libs/output"
	"testing"
)

func TestOutput(t *testing.T) {
	op := output.Output{}
	ret := op.Success("hello")
	t.Log(ret["code"])
	t.Log(ret["message"])
	t.Log(ret["data"])
}
