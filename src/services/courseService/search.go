package courseService

import (
	"cs/src/helper"
	"cs/src/libs/redis"
	"fmt"
)

/**
获取筛选列表
*/
func GetOptionList(city_id string, tid string, optionsKeyValueList map[string]string) map[string][]string {
	var searchFiledNameList []string
	var needQueryFiledNameList []string

	for keyName, _ := range optionsKeyValueList {
		searchFiledNameList = append(searchFiledNameList, keyName)
	}

	//判断哪些字段需要进行搜索
	for _, field := range helper.BASE_DATA_SEARCH_FIELDS {
		needSearch := 1
		for _, searchFiled := range searchFiledNameList {
			if searchFiled == field {
				needSearch = 0
				continue
			}
		}
		if needSearch == 1 {
			needQueryFiledNameList = append(needQueryFiledNameList, field)
		}
	}

	var resultList = make(map[string][]string)

	//获取需要进行搜索的字段结果
	for _, field := range needQueryFiledNameList {
		var optionKeyList []string

		//处理单一的字段结果
		for keyName, keyValue := range optionsKeyValueList {
			optionKey, _ := getkeyPre(city_id, tid, keyName, keyValue)
			currentOptionKey := optionKey + "|" + field
			optionKeyList = append(optionKeyList, currentOptionKey)
		}

		filedResultList, err := redis.RedisClient.SInter(optionKeyList...).Result()
		if err != nil {
			fmt.Println(err)
		}
		resultList[field] = filedResultList
	}

	return resultList
}

/**
获取搜索列表
*/
func GetSearchList(city_id string, tid string, searchKeyValuelist map[string]string) []string {
	var searchKeyList []string
	for keyName, keyValue := range searchKeyValuelist {
		_, searchKey := getkeyPre(city_id, tid, keyName, keyValue)
		searchKeyList = append(searchKeyList, searchKey)
	}

	//直接将所有查询的key值取并集（set并集）返回结果
	searchList, err := redis.RedisClient.SInter(searchKeyList...).Result()
	if err != nil {
		fmt.Println(nil)
	}
	fmt.Println("查询的搜索key列表：", searchKeyList)

	return searchList
}
