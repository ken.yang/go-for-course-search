package courseService

import (
	"cs/src/libs/redis"
	"sync"
)

var waitGroup = sync.WaitGroup{}

/**
开始执行数据
*/
func RunBuild() {
	redisData := GetRedisData()

	//写入redis
	for redisKey, redisValue := range redisData {

		//每一次循环增加一个信号量
		waitGroup.Add(1)

		go addRedisDataToRedis(redisKey, redisValue, &waitGroup)
	}

	//等待所有信号量完成才结束
	waitGroup.Wait()
}

/**
将最终redisData数据写入redis
*/
func addRedisDataToRedis(redisKey string, redisValue []string, wg *sync.WaitGroup) {
	//结束的时候减少信号量
	defer wg.Done()

	//重新组装需要写入的数据，以可以使用sadd方法批量处理
	var saddValue []interface{}
	for _, v := range redisValue {
		saddValue = append(saddValue, v)
	}

	//写入redis
	err := redis.RedisClient.SAdd(redisKey, saddValue...).Err()
	if err != nil {
		panic(err)
	}
}
