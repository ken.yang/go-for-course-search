package courseService

/**
获取搜索的key前缀
*/
func getkeyPre(city_id string, tid string, keyName string, keyValue string) (string1 string, string2 string) {
	//组合key
	keyPre := "city_id_" + city_id + "|tid_" + tid + "|" + keyName + "_" + keyValue
	keyPreOption := "option|" + keyPre
	keyPreSearch := "search|" + keyPre

	return keyPreOption, keyPreSearch
}
