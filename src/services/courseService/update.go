package courseService

import (
	"cs/src/helper"
	"cs/src/libs/redis"
	"cs/src/model"
)

/**
执行更新课程索引
*/
func RunUpdate(ciNew Courser, ciOld *Course) {
	ciNew.Update(ciOld)
}

/**
执行写入课程索引
*/
func RunInsert(ci Courser) {
	ci.Insert()
}

/**
执行删除课程索引
*/
func RunDelete(ci Courser) {
	ci.Delete()
}

/**
课程结构体
*/
type Course struct {
	model.Course
}

/**
课程接口
*/
type Courser interface {
	Insert()
	Update(course *Course)
	Delete()
}

/**
更新课程的索引
*/
func (c *Course) Update(oldCourse *Course) {
	c.Insert()
	oldCourse.Delete()
}

/**
插入新的课程索引
*/
func (c *Course) Insert() {
	for _, keyName := range helper.BASE_DATA_SEARCH_FIELDS {
		keyPreOption, keyPreSearch := getkeyPre(c.Get("city_id"), c.Get("tid"), keyName, c.Get(keyName))

		for _, keyNameItem := range helper.BASE_DATA_SEARCH_FIELDS {
			if keyName == keyNameItem {
				continue
			}
			optionKey := keyPreOption + "|" + keyNameItem
			redis.RedisClient.SAdd(optionKey, c.Get(keyNameItem))

			searchKey := keyPreSearch
			redis.RedisClient.SAdd(searchKey, c.Get("id"))
		}
	}
}

/**
删除课程索引
*/
func (c *Course) Delete() {
	for _, keyName := range helper.BASE_DATA_SEARCH_FIELDS {
		keyPreOption, keyPreSearch := getkeyPre(c.Get("city_id"), c.Get("tid"), keyName, c.Get(keyName))
		for _, keyNameItem := range helper.BASE_DATA_SEARCH_FIELDS {
			if keyName == keyNameItem {
				continue
			}

			optionKey := keyPreOption + "|" + keyNameItem
			redis.RedisClient.SRem(optionKey, c.Get(keyNameItem))

			searchKey := keyPreSearch
			redis.RedisClient.SRem(searchKey, c.Get("id"))
		}
	}
}
