package config

import (
	"cs/src/libs/fileSystem"
	"github.com/vaughan0/go-ini"
	"sync"
)

var config ini.File
var configOnce sync.Once

/**
初始化配置文件
使用单列模式，这样只会执行一次
go单列模式需要使用sync.Once功能
*/
func init() {
	configOnce.Do(func() {
		filePath := fileSystem.GetFilePath("config.ini")

		config, _ = ini.LoadFile(filePath)
	})
}

/**
获取配置文件信息
*/
func Get(section string, key string) string {
	if name, ok := config.Get(section, key); ok {
		return name
	} else {
		panic(ok)
	}
}

/**
获取指定section的值
*/
func Section(section string) map[string]string {
	return config.Section(section)
}
