package helper

import "os"

//基础的固定字段
var BASE_DATA_FIX_FIELDS = []string{"city_id", "tid"}

//基础的可搜索字段
var BASE_DATA_SEARCH_FIELDS = []string{"week", "classroom_id", "campus_id", "project_id", "classtype_id"}

/**
检查文件或目录是否存在
如果由 filename 指定的文件或目录存在则返回 true，否则返回 false
*/
func FileExist(filename string) bool {
	_, err := os.Stat(filename)
	return err == nil || os.IsExist(err)
}
