module cs

go 1.12

require (
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/vaughan0/go-ini v0.0.0-20130923145212-a98ad7ee00ec
)
